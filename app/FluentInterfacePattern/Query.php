<?php


namespace App\FluentInterfacePattern;


use phpDocumentor\Reflection\Types\Integer;

abstract class Query
{
    protected string $table;

    protected array $where;

    protected array $select;

    /**
     * @return static
     */
    public static function Query(): static
    {
        return new static();
    }

    /**
     * @param array|string[] $columns
     * @return $this
     */
    public function select(array $columns = ['*']): static
    {
        $this->select = $columns;

        return $this;
    }

    /**
     * @param array|string $columns
     * @param string $separator
     * @param string|float|Integer|null $value
     * @return $this
     */
    public function Where(
        array|string $columns,
        string $separator = '=',
        string|Integer|float $value = null
    ): static
    {
        if (is_string($columns)) {
            /*
             * check the column is string to return
             */

            $this->where[] = "$columns $separator  $value";

        } elseif (is_array($columns)) {

            /*
             * check the column is array to return
             */

            foreach ($columns as $columnName => $value) {
                $this->where[] = "$columnName $separator  $value";
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function get(): string
    {

        return $this->generate();
    }

    /**
     * @return string
     */
    public function first() :string
    {
        return $this->generate() . ' limit 1';
    }

    /**
     * @return string
     */
    private function generate(): string
    {
        if (empty($this->where)) {

            return sprintf(
                "select %s from %s",
                implode(', ', $this->select),
                $this->table,
            );
        }
        return sprintf(
            "select %s from %s  where %s",
            implode(', ', $this->select),
            $this->table,
            rtrim(implode(' AND ', $this->where), ',')
        );
    }


}