<?php

namespace App\FluentInterfacePattern;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{

    public function testCanGetAllUsers()
    {
        $users = User::Query()
            ->select(['name', 'id'])
            ->Where([
                'name' => 'ahmad',
                'email' => 'a@gmail'
            ])
            ->where('age', '>', 15)
            ->get();

        $this->assertEquals($this->expectedAllUsersQuery(), $users);
    }

    public function testCanGetAllUsersWithoutWhere()
    {
        $users = User::Query()
            ->select(['name', 'id'])
            ->get();

        $this->assertEquals($this->expectedAllUsersWithoutWhere(), $users);
    }

    public function testCanGetFirstUsersWithoutWhere()
    {
        $users = User::Query()
            ->select(['name', 'id'])
            ->first();

        $this->assertEquals($this->expectedFirstUsersWithoutWhere(), $users);
    }


    private function expectedAllUsersQuery(): string
    {
        return 'select name, id from User  where name =  ahmad AND email =  a@gmail AND age >  15';
    }

    private function expectedAllUsersWithoutWhere(): string
    {
        return 'select name, id from User';
    }

    private function expectedFirstUsersWithoutWhere(): string
    {
        return 'select name, id from User limit 1';
    }
}
