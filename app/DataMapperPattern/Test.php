<?php

namespace App\DataMapperPattern;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{

    public function test_can_map_data()
    {
        $storage = new Storage([
            1 => [
                'username' => 'ahmad',
                'email' => 'ahmad@gmail.com'
            ]
        ]);

        $map = new DataMapper($storage);

        $user = $map->findById(1);

        var_dump($user);

        $this->assertInstanceOf(User::class, $user);


    }
}
