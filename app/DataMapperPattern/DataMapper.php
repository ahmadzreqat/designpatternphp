<?php


namespace App\DataMapperPattern;


use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;

class DataMapper
{
    public function __construct(private Storage $storage)
    {
    }

    public function findById(int $id): User
    {
        $result = $this->storage->find($id);

        if ($result === null) {
            throw new InvalidArgumentException(get_called_class() . " #$id not found");
        }

        return $this->map($result);
    }

    #[Pure] private function map(array $row): User
    {
        return User::from($row);
    }
}