<?php


namespace App\DataMapperPattern;


class Storage
{
    public function __construct(private array $data)
    {
    }

//    public function __set($property, $value)
//    {
//        $mergedData = call_user_func_array('array_merge', $this->data);
//
//        foreach ($mergedData as $key => $mergedDatum) {
//            if (is_int($mergedDatum) && is_int($key)) {
//                continue ;
//            }
//
//            $this->$key = $value ;
//        }
//
//    }

    /**
     * @param int $id
     * @return array|null
     */
    public function find(int $id): ?array
    {
        return $this->data[$id] ?? null;
    }
}