<?php

namespace App\SingleTonPattern;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{


    public function testCanConnectAndCreateOnlyOneInstanceOfConnection()
    {


        DbConnection::getInstance();
        DbConnection::getInstance();
        DbConnection::getInstance();
        DbConnection::getInstance();
        DbConnection::getInstance();

        $instance = DbConnection::getInstance();
        $conn = $instance->getConnection();
        var_dump($conn);

        $this->assertTrue(true);
    }
}
