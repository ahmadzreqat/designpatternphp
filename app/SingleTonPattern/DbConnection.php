<?php


namespace App\SingleTonPattern;


use PDO;

class DbConnection
{


    private static self|null $instance = null;
    private PDO $connection;

    private const  HOST = 'localhost';
    private const  USER = 'root';
    private const  PASS = '';
    private const  NAME = 'hr_1';

    /**
     * DbConnection constructor.
     * just example for implement  singleton pattern only
     * we can add try and catch and stop server when we have ANY
     * failed or warning query
     */
    private function __construct()
    {
        $dsn = sprintf("mysql:host=%s; dbname=%s", self::HOST, self::NAME);

        $this->connection = new PDO($dsn, self::USER, self::PASS);
    }

    /**
     * disable clone class
     */
    private function __clone(): void{}

    /**
     * disable serialize class
     */
    private function __wakeup(): void{}

    /**
     * @return DbConnection
     * single ton is check the prop if has value
     * it will returned else will created once
     */
    public static function getInstance(): DbConnection
    {
        if (!self::$instance) {
            echo 'this message will present only one time as u see  ' . PHP_EOL;
            return self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return PDO|string
     * access the connection from $connection prop
     */
    public function getConnection(): PDO|string
    {
        return $this->connection;
    }

}