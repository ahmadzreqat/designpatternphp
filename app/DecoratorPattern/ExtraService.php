<?php


namespace App\DecoratorPattern;


use App\DecoratorPattern\BookingService\BreakFast;
use App\DecoratorPattern\BookingService\Dinner;
use App\DecoratorPattern\BookingService\Lunch;
use http\Exception\InvalidArgumentException;

class ExtraService extends BookingAbstract implements perDaysInterface
{
    /**
     * @var array|string[]
     */
    private array $availableService = [
        'breakfast' => BreakFast::class,
        'lunch' => Lunch::class,
        'dinner' => Dinner::class,
    ];

    /**
     * @var array
     */
    private array $requiredServices;

    /**
     * @param string $service
     * @return $this
     */
    public function add(string $service): static
    {
        if (!array_key_exists($service, $this->availableService)) {
            throw new  InvalidArgumentException("this service $service is not exists .");
        }

        $this->requiredServices[] = 'App\\DecoratorPattern\\BookingService\\' . ucfirst($service);

        return $this;
    }

    public function getPerDays()
    {
        return $this->booking->getPerDays();
    }

    /**
     * @return int
     */
    public function price(): int
    {
        $extraPrice = 0;

        foreach ($this->requiredServices as $requiredService) {
            $extraPrice += $this->getPerDays() * (new $requiredService)->price();
        }

        return $this->booking->price() + $extraPrice;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        $descriptions = null;

        foreach ($this->requiredServices as $requiredService) {
            $descriptions .= (new $requiredService())->description();
        }

        return $this->booking->description() . $descriptions;
    }


}