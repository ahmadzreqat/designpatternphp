<?php


namespace App\DecoratorPattern;


class DoubleBedRoom implements BookingInterface, perDaysInterface
{

    /**
     * DoubleBedRoom constructor.
     * @param int $perDay
     */
    public function __construct(private int $perDay = 1)
    {

    }

    public function getPerDays(): int
    {
        return $this->perDay;
    }

    private function PricePerDay(): int
    {
        return $this->perDay * 200;
    }

    public function price(): int
    {
        return $this->PricePerDay();
    }

    public function description(): string
    {
        $isDay = $this->perDay > 1 ? 'Days' : 'Day';
        return "double bed room for $this->perDay " . $isDay;
    }
}