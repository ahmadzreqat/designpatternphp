<?php


namespace App\DecoratorPattern\BookingService;


use App\DecoratorPattern\BookingInterface;

class BreakFast implements BookingInterface
{

    public function price(): int
    {
        return 10 ;
    }

    public function description() : string
    {
        return ' with break fast' ;
    }
}