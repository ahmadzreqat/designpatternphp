<?php


namespace App\DecoratorPattern\BookingService;


use App\DecoratorPattern\BookingInterface;

class Lunch implements BookingInterface
{

    public function price(): int
    {
        return 12 ;
    }

    public function description() : string
    {
        return ' with lunch' ;
    }

}