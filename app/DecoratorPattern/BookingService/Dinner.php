<?php


namespace App\DecoratorPattern\BookingService;


use App\DecoratorPattern\BookingInterface;

class Dinner implements BookingInterface
{

    public function price(): int
    {
        return 15 ;
    }

    public function description() : string
    {
        return ' with Dinner' ;
    }

}