<?php


namespace App\DecoratorPattern;


interface BookingInterface
{
    public function price(): int ;

    public function description(): string ;
}