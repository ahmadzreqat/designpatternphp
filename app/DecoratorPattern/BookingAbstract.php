<?php


namespace App\DecoratorPattern;


abstract class BookingAbstract implements BookingInterface
{

    public function __construct(protected BookingInterface $booking)
    {
    }
}