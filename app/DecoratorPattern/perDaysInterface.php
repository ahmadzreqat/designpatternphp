<?php


namespace App\DecoratorPattern;


interface perDaysInterface
{

    public function getPerDays();
}