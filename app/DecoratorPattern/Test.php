<?php

namespace App\DecoratorPattern;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{


    public function test_can_booking_single_bed_room()
    {
        $booking = new SingleBedRoom(5);

        $extraBooking = new ExtraService($booking);
        $extraBooking
            ->add('lunch')
            ->add('dinner')
            ->add('breakfast');

        var_dump([
            'total_price' => $extraBooking->price(),
            'full_booking_information' => $extraBooking->description()
        ]);
        $this->assertSame(650, $extraBooking->price());

    }


    public function test_can_booking_double_room()
    {
        $booking = new DoubleBedRoom(5);

        $extraBooking = new ExtraService($booking);
        $extraBooking
            ->add('lunch')
            ->add('dinner');

        var_dump([
            'total_price' => $extraBooking->price(),
            'full_booking_information' => $extraBooking->description()
        ]);
        $this->assertSame(1135, $extraBooking->price());

    }
}
