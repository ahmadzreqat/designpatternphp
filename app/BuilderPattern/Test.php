<?php

namespace App\BuilderPattern;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{


    public function testCanBuildCar()
    {
        $carBuilder = new CarBuilder();

        $newVehicle = (new Director())->build($carBuilder);

        echo $newVehicle->createdVehicle();

        $this->assertInstanceOf(Car::class, $newVehicle);

    }
}
