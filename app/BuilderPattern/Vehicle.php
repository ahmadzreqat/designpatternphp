<?php


namespace App\BuilderPattern;


abstract class Vehicle
{

    private array $parts;

    public function set(string $key, object $value): string
    {
        $name = get_class($value);

        return $this->parts[$name][] = $key;
    }

    public function createdVehicle(): string
    {
        $description = null;
        foreach ($this->parts as $partName => $partValues) {
            $description .= PHP_EOL . (new $partName)->getName() . ': ';
            $description .= implode(' ', $partValues) . PHP_EOL;
        }

        return 'the Created Vehicle : ' . $description;
    }
}