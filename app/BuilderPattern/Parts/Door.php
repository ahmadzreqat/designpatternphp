<?php


namespace App\BuilderPattern\Parts;


class Door implements getClassNameInterface
{

    public function getName(): string
    {
        return 'Door';
    }
}