<?php


namespace App\BuilderPattern\Parts;


interface getClassNameInterface
{

    public function getName();

}