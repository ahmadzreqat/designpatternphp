<?php


namespace App\BuilderPattern\Parts;


class Wheel implements getClassNameInterface
{

    public function getName(): string
    {
        return 'Wheel';
    }
}