<?php


namespace App\BuilderPattern\Parts;


class Engine implements getClassNameInterface
{

    public function getName(): string
    {
        return 'Engine';
    }
}