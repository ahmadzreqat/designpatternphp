<?php


namespace App\BuilderPattern;


use App\BuilderPattern\Parts\Door;
use App\BuilderPattern\Parts\Engine;
use App\BuilderPattern\Parts\Wheel;

class CarBuilder implements Builder
{
    private Car $car;

    public function create()
    {
        $this->car = new Car();
    }

    public function addWheel()
    {
        $this->car->set('ll', new Wheel());
        $this->car->set('lr', new Wheel());
        $this->car->set('rl', new Wheel());
        $this->car->set('rr', new Wheel());
    }

    public function addEngine()
    {
        $this->car->set('Engine', new Engine());

    }

    public function addDoors()
    {
        $this->car->set('frontRightDoor', new Door());
        $this->car->set('frontLeftDoor', new Door());
        $this->car->set('backLeftDoor', new Door());
        $this->car->set('backRightDoor', new Door());
    }

    public function getVehicle(): Vehicle
    {
        return $this->car;
    }
}