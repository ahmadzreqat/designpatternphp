<?php


namespace App\BuilderPattern;


class Director
{
    public function build(Builder $builder): Vehicle
    {
        // create a new car object
        $builder->create();
        // add doors parts
        $builder->addDoors();
        //add engine to car
        $builder->addEngine();
        // add wheels to car
        $builder->addWheel();
        // create a vehicle
        return $builder->getVehicle();

    }
}