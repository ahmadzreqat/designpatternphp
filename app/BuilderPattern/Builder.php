<?php


namespace App\BuilderPattern;


interface Builder
{
    public function create();

    public function addWheel();

    public function addEngine();

    public function addDoors();

    public function getVehicle(): Vehicle;
}