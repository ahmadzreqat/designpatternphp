<?php

namespace App\FacadePattern;

use Exceptions\InvalidHashException;
use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    /**
     * @throws InvalidHashException
     */
    public function test_can_create_hash()
    {
        $hash = Hash::make('123', 'bcrypt');

        var_dump($hash);

        $this->assertTrue(true, $hash);
    }
}
