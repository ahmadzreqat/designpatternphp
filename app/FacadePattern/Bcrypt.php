<?php


namespace App\FacadePattern;


class Bcrypt implements HashInterface
{

    public function make(string $string): string
    {
        return password_hash($string, PASSWORD_DEFAULT);
    }

}