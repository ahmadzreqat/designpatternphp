<?php


namespace App\FacadePattern;


class MD5 implements HashInterface
{

    public function make(string $string): string
    {
        return md5($string);
    }

}