<?php


namespace App\FacadePattern;


use Exceptions\InvalidHashException;

class Hash
{
    /**
     * @var array|string[]
     *  its just an example about facade
     */
    private static array $availableHash = [
        'base64encode' => Base64Encode::class,
        'md5' => MD5::class,
        'sha1' => SHA1::class,
        'bcrypt' => Bcrypt::class
    ];

    /**
     * @param $string
     * @param $hashName
     * @return mixed
     * @throws InvalidHashException
     */
    public static function make($string, $hashName): mixed
    {
        if (!array_key_exists($hashName, (self::$availableHash))) {
            return throw new  InvalidHashException("the $hashName is unknown");
        }

        return (new self::$availableHash[$hashName])->make($string);
    }

}