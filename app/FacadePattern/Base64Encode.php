<?php


namespace App\FacadePattern;


class Base64Encode implements HashInterface
{

    public function make(string $string): string
    {
        return base64_encode($string);
    }
}