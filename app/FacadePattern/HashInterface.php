<?php


namespace App\FacadePattern;


interface HashInterface
{
   public function make(string $string): string;
}