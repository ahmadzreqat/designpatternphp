<?php


namespace App\FacadePattern;


class SHA1 implements HashInterface
{

    public function make(string $string): string
    {
        return sha1($string);
    }
}