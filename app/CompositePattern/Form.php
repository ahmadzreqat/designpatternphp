<?php


namespace App\CompositePattern;



class Form implements renderInterface
{
    private string $form = '<form>';
    private array $elements = [];
    private string $closeForm = '</form>';


    public function add(renderInterface $element): static
    {
        $this->elements[] = $element;

        return $this;
    }

    public function render(): string
    {
        $formBuilder = $this->form;

        foreach ($this->elements as $element) {
            $formBuilder .= PHP_EOL . $element->render();
        }

        return $formBuilder . PHP_EOL . $this->closeForm;
    }


}