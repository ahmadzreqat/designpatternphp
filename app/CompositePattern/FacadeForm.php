<?php


namespace App\CompositePattern;


use JetBrains\PhpStorm\Pure;

class FacadeForm
{
    private Form $form;

    #[Pure] public function __construct()
    {
        $this->form = new Form();
    }

    public function addInput($label, $inputType): static
    {
        $this->form->add(new Input(new Label($label), new InputType($inputType)));

        return $this;
    }


    public function addSelect($label, array $options): static
    {

        if ($options) {

            $select = new Select(new Label($label));
            foreach ($options as $value => $title) {
                $select->addOption($value, $title);
            }
            $this->form->add($select);
            return $this;
        }


        $this->form->add((new Select(new Label($label))));

        return $this;
    }

    public function render(): string
    {
        return $this->form->render();
    }

    public function __toString(): string
    {
        return $this->render();
    }

    /**
     *  render form on destruct the form
     */
    public function __destruct()
    {
        $this->form->render();
    }


}