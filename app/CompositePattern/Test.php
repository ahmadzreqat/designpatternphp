<?php

namespace App\CompositePattern;

use PHPUnit\Framework\TestCase;

/**
 * Class Test
 * @package App\CompositePattern
 *
 *  implement Composite Pattern on Html Form
 *  Work on PHP 8 VERSION ONLY
 */
class Test extends TestCase
{


    public function test_can_create_form()
    {
        $form = new Form();
        $form
            ->add(
                new Input(new Label('Name'), new InputType('text'))
            )->add(
                new Input(new Label('Email'), new InputType('email'))
            )->add(
                (new Select(new Label('Category')))
                    ->addOption(1, 'cat1')
                    ->addOption(2, 'cat2')
            );


        $this->assertEquals(
            $this->expectedForm(),
            $form->render());

    }


    public function testFacadeForm()
    {
        $form = new   FacadeForm();

        $form
            ->addInput('Name', 'text')
            ->addInput('Email', 'email')
            ->addSelect('Category',/* addOptions  */ [
                1 => 'cat1',
                2 => 'cat2'
            ]);
        echo($form->render());

        $this->assertEquals(
            $this->expectedForm(),
            $form);

    }

    private function expectedForm(): string
    {
        return '<form>
Name <input type="text" />
Email <input type="email" />
Category <select><option value="1">cat1</option>
<option value="2">cat2</option></select>
</form>';
    }
}
