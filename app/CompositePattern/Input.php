<?php


namespace App\CompositePattern;


use JetBrains\PhpStorm\Pure;

class Input implements renderInterface
{

    public function __construct(public Label $label, public InputType $type)
    {

    }

    #[Pure] public function render(): string
    {
        return $this->label->render() . ' ' . "<input type=\"{$this->type->render()}\" />";
    }
}