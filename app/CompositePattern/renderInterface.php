<?php


namespace App\CompositePattern;


interface renderInterface
{
    public function render();
}