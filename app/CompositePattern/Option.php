<?php


namespace App\CompositePattern;


class Option implements renderInterface
{

    private string $option = "<option value=\"%s\">%s</option>";

    public function __construct(private string $value, private string $title)
    {

    }

    public function render(): string
    {
        return sprintf($this->option, $this->value, $this->title);
    }

}