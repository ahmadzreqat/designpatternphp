<?php


namespace App\CompositePattern;


use JetBrains\PhpStorm\Pure;

class Select implements renderInterface
{
    private array $options = [];

    public function __construct(public Label $label)
    {

    }

    public function addOption($value, $title): static
    {
        $option = new Option($value, $title);

        $this->options[] = $option->render();

        return $this;
    }


    #[Pure] public function render(): string
    {
        $options = implode(PHP_EOL , $this->options);

        return $this->label->render() . ' ' . "<select>$options</select>";
    }
}