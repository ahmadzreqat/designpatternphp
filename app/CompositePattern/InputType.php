<?php


namespace App\CompositePattern;


class InputType implements renderInterface
{
    public function __construct(private string $type){}

    public function render(): string
    {
        return $this->type ;
    }
}