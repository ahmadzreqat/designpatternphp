<?php


namespace App\CompositePattern;


class Label
{

    public function __construct(private string $label)
    {
    }

    public function render(): string
    {
        return $this->label;
    }
}