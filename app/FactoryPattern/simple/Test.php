<?php

namespace App\FactoryPattern\simple;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{


    public function testCanCreateCar()
    {
       $created =  CarFactory::create('bmw' , 2020);


        $this->assertEquals('bmw 2020' , $created);
    }

}
