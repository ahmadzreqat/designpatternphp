<?php


namespace App\FactoryPattern\simple;

class CarFactory
{


    public static function create(string $name, string $model): string
    {
        return (new Car($name, $model))->getCar();
    }

}