<?php


namespace App\FactoryPattern\simple;


class Car
{

    public function __construct(private string $name, private string $model)
    {
    }

    public function getCar(): string
    {
        return $this->name . ' ' . $this->model;
    }

}