<?php


namespace App\FactoryPattern\staticFactory;


use http\Exception\InvalidArgumentException;

class BookFactory
{
    private static array $existingBooks = [
        'php' => Php::class,
        'js' => Js::class,
        'Py' => Py::class,
        'java' => Java::class
    ];


    public static function create($bookName)
    {
        if (!array_key_exists($bookName, self::$existingBooks)) {
            throw new InvalidArgumentException("this $bookName book is not exist yet");
        }

        $class = 'App\\FactoryPattern\\staticFactory\\' . ucfirst($bookName);

        return (new $class)->getDescription();
    }

}