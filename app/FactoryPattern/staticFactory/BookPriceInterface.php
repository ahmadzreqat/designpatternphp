<?php


namespace App\FactoryPattern\staticFactory;


interface BookPriceInterface
{
    public function price(): int;
}