<?php


namespace App\FactoryPattern\staticFactory;


class Java extends Book implements BookPriceInterface
{

    public string $author = 'Java author';

    public string $subject = 'Java programming language';


    public function price(): int
    {
        return 100;
    }

    public function getDescription(): string
    {
        return $this->author . ' ' . $this->subject . ' ' . 'total price ' . $this->price();
    }
}