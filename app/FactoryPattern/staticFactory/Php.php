<?php


namespace App\FactoryPattern\staticFactory;


class Php extends Book implements BookPriceInterface
{

    public string $author = 'php author';

    public string $subject = 'php programming language';


    public function price(): int
    {
        return 100;
    }

    public function getDescription(): string
    {
        return $this->author . ' ' . $this->subject . ' ' . 'total price ' . $this->price();
    }
}