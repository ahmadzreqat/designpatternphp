<?php


namespace App\FactoryPattern\staticFactory;


class Py extends Book
{
    public string $author = 'Py author';

    public string $subject = 'Py programming language';

    public function getDescription(): string
    {
        return $this->author . ' ' . $this->subject;
    }
}