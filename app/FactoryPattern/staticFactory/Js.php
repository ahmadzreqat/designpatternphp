<?php


namespace App\FactoryPattern\staticFactory;


class Js extends Book
{

    public string $author = 'js author';

    public string $subject = 'js programming language';


    public function getDescription(): string
    {
      return $this->author . ' ' . $this->subject ;
    }
}