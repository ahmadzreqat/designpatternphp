<?php


namespace App\FactoryPattern\staticFactory;


abstract class Book
{
    public string  $subject ;
    public string  $author ;

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }


    abstract public function getDescription();

}