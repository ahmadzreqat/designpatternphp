<?php

namespace App\FactoryPattern\staticFactory;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{


    public function testCanCreateNewBookWithDescription()
    {
        echo PHP_EOL . BookFactory::create('php') . PHP_EOL;

        echo BookFactory::create('js') . PHP_EOL;

        echo BookFactory::create('java');

        $this->assertEquals((new Php())->getDescription() ,  BookFactory::create('php') );
    }
}
